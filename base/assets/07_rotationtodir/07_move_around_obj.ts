// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    target: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:
    
    // onLoad () {}
    start () {

    }

    update (dt) {
        //计算出当前位置相对于X的偏移
        var dx = this.node.x - this.target.x;
        var dy = this.node.y - this.target.y;
        var dir = cc.v2(dx,dy);
        dir = dir.rotate(dt * 0.5);

        this.node.x = dir.x;
        this.node.y = dir.y;

        var angle = dir.signAngle(cc.v2(1,0));
        var degree = angle / Math.PI * 180;
        this.node.rotation = degree - 90;
    }
}
